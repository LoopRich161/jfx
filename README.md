# How to build and launch:
1. Fork project
2. Run `mvn install` (!) to build project and it's dependencies
3. Run `mvn -pl :launcher javafx:run`

# How to launch from IDE:
1. Use this class as main class `dk.xakeps.jfx.core.app.Main`
2. Use classpath of module `launcher`