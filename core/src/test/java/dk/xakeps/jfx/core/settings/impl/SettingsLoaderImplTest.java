/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.settings.impl;

import dk.xakeps.jfx.core.settings.Settings;
import dk.xakeps.jfx.core.settings.SettingsLoader;
import javafx.beans.property.StringProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class SettingsLoaderImplTest {

    private SettingsLoader loader;
    private Settings config;

    @BeforeEach
    public void setup() throws IOException {
        Path root = Paths.get("").toAbsolutePath();
        loader = new SettingsLoaderImpl(root);
        config = loader.load(Paths.get("config"));
    }

    @Test
    public void testLoad() throws IOException {
        config.setProperty("string", "value");
        assertEquals("value", config.getStringProperty("string", null).get());

        config.setProperty("int", Integer.MAX_VALUE);
        assertEquals(Integer.MAX_VALUE, config.getIntProperty("int", 0).get());

        config.setProperty("long", Long.MAX_VALUE);
        assertEquals(Long.MAX_VALUE, config.getLongProperty("long", 0).get());

        config.save();
        setup();

        assertEquals("value", config.getStringProperty("string", null).get());
        assertEquals(Integer.MAX_VALUE, config.getIntProperty("int", 0).get());
        assertEquals(Long.MAX_VALUE, config.getLongProperty("long", 0).get());
    }

    @Test
    public void testChange() {
        config.setProperty("string", "value");
        assertEquals("value", config.getStringProperty("string", null).get());

        config.setProperty("string", "changed");
        assertEquals("changed", config.getStringProperty("string", null).get());
    }

    @Test
    public void testNoSave() throws IOException {
        config.setProperty("string", "value");
        assertEquals("value", config.getStringProperty("string", null).get());
        config.save();

        config.setProperty("string", "wrong");
        setup();
        assertEquals("value", config.getStringProperty("string", null).get());
    }

    @Test
    public void testBinding() {
        config.setProperty("string", "value");
        assertEquals("value", config.getStringProperty("string", null).get());

        StringProperty string = config.getStringProperty("string");
        string.set("new-value");
        assertEquals("new-value", config.getStringProperty("string", null).get());

    }
}