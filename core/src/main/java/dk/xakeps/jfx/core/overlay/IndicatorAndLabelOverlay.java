/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.overlay;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class IndicatorAndLabelOverlay implements Overlay {
    private final IndicatorAndLabel content = new IndicatorAndLabel();

    @Override
    public Node getContent() {
        return content;
    }

    public ProgressIndicator getProgressIndicator() {
        return content.progressIndicator;
    }

    public Label getLabel() {
        return content.label;
    }

    public double getSpacing() {
        return content.container.getSpacing();
    }

    public void setSpacing(double value) {
        content.container.setSpacing(value);
    }

    public DoubleProperty spacingProperty() {
        return content.container.spacingProperty();
    }

    public Background getBackground() {
        return content.container.getBackground();
    }

    public void setBackground(Background value) {
        content.container.setBackground(value);
    }

    public ObjectProperty<Background> backgroundProperty() {
        return content.container.backgroundProperty();
    }

    public static class IndicatorAndLabel extends TilePane {
        private final VBox container = new VBox();
        private final ProgressIndicator progressIndicator = new ProgressIndicator();
        private final Label label = new Label();

        public IndicatorAndLabel() {
            setAlignment(Pos.CENTER);
            container.setSpacing(5);
            container.setAlignment(Pos.CENTER);
            container.setPadding(new Insets(30));
            container.setBackground(new Background(new BackgroundFill(new Color(1, 1, 1, 0.8), new CornerRadii(5), new Insets(10))));
            getChildren().add(container);
            container.getChildren().add(progressIndicator);
            container.getChildren().add(label);
        }
    }
}
