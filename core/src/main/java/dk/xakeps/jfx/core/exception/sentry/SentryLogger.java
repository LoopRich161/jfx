/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.exception.sentry;

import io.sentry.Sentry;

public final class SentryLogger {
    private static final String DSN = "https://1a9bb014a6274000a26bff982d12c5e0@o454679.ingest.sentry.io/5445084";

    public static void init() {
        Sentry.init(DSN);
    }

    public static void log(Exception ex) {
        Sentry.capture(ex);
    }

    public static void log(String message) {
        Sentry.capture(message);
    }

    public static void log(Throwable throwable) {
        Sentry.capture(throwable);
    }

}
