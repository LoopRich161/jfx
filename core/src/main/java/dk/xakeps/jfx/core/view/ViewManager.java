/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.view;

import dk.xakeps.jfx.core.overlay.IndicatorOverlay;
import dk.xakeps.jfx.core.overlay.Overlay;
import dk.xakeps.jfx.core.overlay.OverlayManager;
import dk.xakeps.jfx.core.overlay.RootPane;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;

public class ViewManager {
    private final ObjectProperty<View> currentView = new SimpleObjectProperty<>(this, "currentView");

    private final RootPane rootPane = new RootPane();
    private final OverlayManager overlayManager = new OverlayManager(rootPane);

    public ViewManager(Stage stage) {
        Scene scene = new Scene(rootPane, 640, 480);
        stage.setScene(scene);
    }

    public View getCurrentView() {
        return currentView.getValue();
    }

    public void setCurrentView(View view) {
        // only enable overlay if it was not enabled
        boolean savedState = overlayManager.isOverlayEnabled();
        if(!savedState) {
            overlayManager.enableOverlay(IndicatorOverlay.INSTANCE);
        }
        rootPane.setContent(view.getContent());
        currentView.set(view);
        if (!savedState) {
            overlayManager.disableOverlay();
        }
    }

    public OverlayManager getOverlayManager() {
        return overlayManager;
    }

    // Maybe something better?
    public <T> CompletableFuture<T> runWithOverlay(Overlay overlay, CompletableFuture<T> task) {
        overlayManager.enableOverlay(overlay);
        return task.whenCompleteAsync((t, throwable) -> overlayManager.disableOverlay(), Platform::runLater);
    }

    public ObservableValue<View> currentViewProperty() {
        return currentView;
    }
}
