/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SoKnight
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.settings;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.StringProperty;

import java.io.IOException;

public interface Settings {
    
    StringProperty getStringProperty(String key);
    
    default StringProperty getStringProperty(String key, String def) {
        StringProperty property = getStringProperty(key);
        if (!hasProperty(key)) {
            property.setValue(def);
        }
        return property;
    }
    
    IntegerProperty getIntProperty(String key);
    
    default IntegerProperty getIntProperty(String key, int def) {
        IntegerProperty property = getIntProperty(key);
        if (!hasProperty(key)) {
            property.setValue(def);
        }
        return property;
    }
    
    LongProperty getLongProperty(String key);
    
    default LongProperty getLongProperty(String key, long def) {
        LongProperty property = getLongProperty(key);
        if (!hasProperty(key)) {
            property.setValue(def);
        }
        return property;
    }
    
    boolean hasProperty(String key);
    
    default Settings setProperty(String key, String value) {
        getStringProperty(key).set(value);
        return this;
    }
    default Settings setProperty(String key, int value) {
        getIntProperty(key).set(value);
        return this;
    }
    default Settings setProperty(String key, long value) {
        getLongProperty(key).set(value);
        return this;
    }

    void save() throws IOException;
}
