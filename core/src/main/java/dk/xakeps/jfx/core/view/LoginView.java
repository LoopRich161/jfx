/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class LoginView implements View {
    private final AnchorPane root;

    {
        root = new AnchorPane();
        root.getStylesheets().add("/css/common.css");
        root.setPadding(new Insets(5, 5, 5, 5));
        
        FlowPane main = new FlowPane();
        
        AnchorPane.setTopAnchor(main, 0d);
        AnchorPane.setRightAnchor(main, 0d);
        AnchorPane.setBottomAnchor(main, 0d);
        AnchorPane.setLeftAnchor(main, 0d);
        
        root.getChildren().add(main);
        
        main.setAlignment(Pos.CENTER);
        main.setColumnHalignment(HPos.CENTER);
        main.setOrientation(Orientation.VERTICAL);
        main.setPrefWrapLength(0);
        
        ImageView logo = new ImageView(new Image(getClass().getResourceAsStream("/logo.png")));
        logo.setFitWidth(200);
        logo.setFitHeight(150);
        logo.setPickOnBounds(true);
        logo.setPreserveRatio(true);
        main.getChildren().add(logo);

        FlowPane controls = new FlowPane();
        main.getChildren().add(controls);
        controls.setVgap(5);
        controls.setAlignment(Pos.CENTER);
        controls.setPrefWrapLength(0);

        VBox fields = new VBox();
        controls.getChildren().add(fields);
        fields.setSpacing(5);
        fields.setAlignment(Pos.CENTER);

        TextField usernameField = new TextField();
        usernameField.setPromptText("Username");
        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("Password");
        fields.getChildren().addAll(usernameField, passwordField);

        HBox buttons = new HBox();
        controls.getChildren().add(buttons);
        buttons.setSpacing(5);
        buttons.setAlignment(Pos.CENTER);

        CheckBox rememberMeCheckBox = new CheckBox("Remember me");
        Button logInButton = new Button("Log In");
        buttons.getChildren().addAll(rememberMeCheckBox, logInButton);
    }

    @Override
    public Node getContent() {
        return root;
    }

    @Override
    public boolean isLoaded() {
        return true;
    }
}
